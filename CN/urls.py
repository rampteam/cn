from django.conf.urls import patterns, url
# from django.contrib import admin

urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', 'CN.views.home', name='home'),
    url(r'^h1/problema1$', 'homework1.views.problem_one', name='problem_one'),
    url(r'^h1/problema1_response$', 'homework1.views.problem_one_response', name='problem_one_response'),
    url(r'^h1/problema2$', 'homework1.views.problem_two', name='problem_two'),
    url(r'^h1/problema2_response$', 'homework1.views.problem_two_response', name='problem_two_response'),
    url(r'^h1/problema3$', 'homework1.views.problem_three', name='problem_three'),
    url(r'^h2$', 'homework2.views.homework_two', name='homework_two'),
    url(r'^h3$', 'homework3.views.homework_three', name='homework_three'),
    url(r'^h4$', 'homework456.views.homework_four', name='homework_four'),
    url(r'^h5$', 'homework456.views.homework_five', name='homework_five'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)
