import numpy
import math


class FourRussiansMult(object):

    """Four Russians Multiplication method for square matrix"""

    def init(self, A, B):
        """
        validate matrixes and and 0 padding for log n power
        :param A:
        :param B:
        :return:
        """

        # initially padding number is 0
        self.padding = 0

        n_rows, n_cols = A.shape
        if A.shape != B.shape or n_rows != n_cols:
            raise Exception("Expecting nxn dim matrix!")
        k = math.floor(math.log(n_rows, 2))
        r = n_rows - (k * math.floor(n_rows / k))
        if r != 0:
            self.padding = r
            A = self.pad_zeroes(A, self.padding)
            B = self.pad_zeroes(B, self.padding)
            n_rows, n_cols = A.shape

        self.n = n_rows

        return (A, B)

    def pad_zeroes(self, A, nr):
        """
        Add nr zero padded columns and lines

        Returns:
            new padded matrix
        """
        n_rows, n_cols = A.shape
        rpad = numpy.zeros((n_cols + nr), numpy.int8)
        cpad = numpy.zeros((nr), numpy.int8)
        arr = A.tolist()

        for l in arr:
            l.extend(cpad)
        for i in range(nr):
            arr.append(list(rpad))
        return numpy.asmatrix(arr)

    @property
    def stripe_len(self):
        return math.floor(math.log(self.n, 2))

    @property
    def nr_stripes(self):
        return math.floor(self.n / self.stripe_len)

    def four_method(self, A, B):
        """
        Computes multiplication using Four Russians Method

        Args:
            A, B numpy matrix

        Returns:
            C numpy matrix
        """
        A, B = self.init(
            self.matrix_copy(A),
            self.matrix_copy(B)
        )

        k = self.stripe_len
        # 1 X 1 matrix
        if k == 0:
            return A[0][0] and B[0][0]

        a = self.n
        # create a zero matrix
        C = self.get_zeroed_matrix(self.n, self.n)
        for i in range(self.nr_stripes):
            # 3
            T = self.make_table(B, i)
            # 3
            for j in range(0, a):
                ai = self.read_subline(A, j, i * k, k)
                x = self.get_num(ai)
                self.add_row_to_matrix(C, j, T, x)

        return self.get_result(C)

    def read_subline(self, A, r, c, k):
        l = []
        for x in range(c, c + k):
            l.append(A.item(r, x))

        return l

    def add_row_to_matrix(self, C, r, T, x):
        n_r, n_c = C.shape
        for i in range(n_c):
            C[r, i] += T[x][i]

    def make_table(self, B, i):
        m = self.stripe_len

        T = []
        T.append(self.get_zeroed_line())
        for j in range(1, (2 ** m)):
            k = self.get_binary_power_range(j)
            l = list(T[j - (2 ** k)])
            b = B.tolist()[i * m + k]
            for d in range(self.n):
                l[d] += b[d]

            # T[j]
            T.append(l)

        return T

    def get_num(self, v):
        nr = 0
        for i, e in reversed(list(enumerate(v))):
            nr += (2 ** i) * e

        return nr

    def get_zeroed_matrix(self, n, m):
        mt = numpy.zeros((n, m), numpy.int8)
        return numpy.asmatrix(mt)

    def get_result(self, C):
        """
        Extract final matrix & removes padding if needed
        """

        if not self.padding:
            return C

        arr = C.tolist()
        result = []

        # remove padding
        for i in range(self.n - self.padding):
            result.append(arr[i][:-self.padding])

        return numpy.asmatrix(result)

    def get_zeroed_line(self):
        return [0 for i in range(self.n)]

    def get_binary_power_range(self, nr):
        p = 0
        while not (2 ** p <= nr < 2 ** (p + 1)):
            p += 1

        return p

    def is_power2(self, num):
        """
        States if a number is a power of two
        """
        return num != 0 and ((num & (num - 1)) == 0)

    def next_power_of_2(self, n):
        """
        Return next power of 2 greater than or equal to n
        """
        return 2**(n - 1).bit_length()

    def matrix_copy(self, m):
        return numpy.asmatrix(numpy.copy(m))
