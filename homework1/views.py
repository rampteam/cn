import locale
import json
import numpy
import traceback

from django.shortcuts import render_to_response
from .four_russians import FourRussiansMult

locale.setlocale(locale.LC_ALL, '')


def machine_precision():
    u = 1
    while 1.0 + 10 ** -u != 1.0:
        u += 1
    return u - 1


# Create your views here.
def problem_one(request):
    return render_to_response('homework1/problem_one.html')


def problem_one_response(request):
    return render_to_response('homework1/problem_one_response.html', {'u': machine_precision()})


def problem_two(request):
    return render_to_response('homework1/problem_two.html')


def problem_two_response(request):
    a, b, c = 1846293612, 12312415342, 0.0000008
    first = (a * b) * c
    second = a * (b * c)
    resp = {
        'a': locale.format('%d', a, grouping=True),
        'b': locale.format('%d', b, grouping=True),
        'c': format(0.0000008, '.7f'),
        'first': first,
        'second': second
    }
    return render_to_response('homework1/problem_two_response.html', resp)


def problem_three(request):
    result = {}
    try:
        m1 = request.GET.get('matrix_one')
        m2 = request.GET.get('matrix_two')
        if m1 and m2:
            m1 = json.loads(m1)
            m2 = json.loads(m2)
            m1 = numpy.matrix(m1)
            m2 = numpy.matrix(m2)
            o = FourRussiansMult()
            c = o.four_method(m1, m2)
            c = numpy.array(c).tolist()
            result['response'] = json.dumps(c)
    except Exception as e:
        print(e)
        traceback.print_exc()
        result['error'] = str(e)
    return render_to_response('homework1/problem_three.html', result)
