import numpy
from decimal import *


class LS:
    def __init__(self, eps=None):
        if eps is None:
            eps = 10 ** -4
        self.eps = eps

    def lu(self, A):
        A = (self.make_1dx_based(A))
        n, n = A.shape
        for p in range(1, n):
            for i in range(p, n):
                a = A[p, i]
                for k in range(1, p):
                    a = a - A[p, k] * A[k, i]
                A[p, i] = a
            for i in range(p + 1, n):
                a = A[i, p]
                for k in range(1, p):
                    a = a - A[i, k] * A[k, p]
                self.check_divz(A[p, p], 'LU')
                A[i, p] = a / A[p, p]
        return self.make_0dx_based(A)

    def det_of_ludecomposition(self, A):
        # it is the product of the first diagonal
        # https://proofwiki.org/wiki/Determinant_of_Triangular_Matrix
        n, n = A.shape
        d = 1
        for p in range(n):
            d *= A[p, p]
        return d


    def direct_subsitution(self, A, b, unit_diag=True):
        self.validate_substitution(A)
        A, n, x, b = self.unit_based_substitution(A, b)
        for i in range(1, n):
            bi = b[i]
            for j in range(1, i):
                bi = bi - A[i, j] * x[j]
            xi = bi
            if not unit_diag:
                self.check_divz(A[i, i])
                xi = bi / A[i, i]
            x[i] = xi

        x.pop(0)
        return x

    def inverse_subsitution(self, A, b):
        self.validate_substitution(A)
        A, n, x, b = self.unit_based_substitution(A, b)

        for i in reversed(range(1, n)):
            bi = b[i]
            for j in range(i + 1, n):
                bi = bi - A[i, j] * x[j]

            self.check_divz(A[i, i])
            xi = bi / A[i, i]
            x[i] = xi

        x.pop(0)
        return x

    def validate_substitution(self, A):
        # matricea trebuie sa fie nesingulara
        if self.det_of_ludecomposition(A) == 0:
            raise Exception("Matricea trebuie sa fie nesingulara!")

    def unit_based_substitution(self, A, b):
        # make everything 1 dx based
        A = (self.make_1dx_based(A))
        n, n = A.shape
        x = [0] * n
        b.insert(0, 0)
        return (A, n, x, b)

    def verify_solution(self, A, x, b):
        n, n = A.shape
        y = [0.0 for x in range(n)]
        e = list(y)
        for i in (range(n)):
            for j in (range(n)):
                y[i] += A[i, j] * x[j]
        for i in (range(n)):
            e[i] = y[i] - b[i]

        err = 0.0
        for i in (range(n)):
            err += e[i] ** 2
        return Decimal(err).sqrt()

    def make_1dx_based(self, A):
        """
        Add zero padded column

        Returns:
            new padded matrix
        """
        nr = 1
        n_rows, n_cols = A.shape
        rpad = numpy.zeros((n_cols + nr), numpy.int8)
        arr = A.tolist()

        for l in arr:
            l.insert(0, 0)

        arr.insert(0, list(rpad))
        return numpy.asmatrix(arr)

    def make_0dx_based(self, A):
        """
        Remove zero padded column

        Returns:
            new padded matrix
        """
        n, n_cols = A.shape
        arr = A.tolist()
        result = []

        nr = 1
        # remove padding
        for i in range(1, n):
            result.append(arr[i][nr:])

        return numpy.asmatrix(result)

    def check_divz(self, nr, location=''):
        if not abs(nr) > self.eps:
            raise Exception("Cannot divide by zero! {}".format(location))

    def matrix_copy(self, m):
        return numpy.asmatrix(numpy.copy(m))

