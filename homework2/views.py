import json
import numpy
import traceback

from django.shortcuts import render_to_response
from .lu_decomposition import LS


def homework_two(request):
    context = {
        'epsilon': int(request.GET.get('epsilon', 5) if request.GET.get('epsilon', 5) else 0)
    }
    try:
        ls = LS(10 ** - context['epsilon'])
        b = json.loads(request.GET.get('matrix_two'))
        b = [item for sublist in b for item in sublist]

    except Exception as e:
        return render_to_response('homework2/problem.html', context)
    try:
        m1 = json.loads(request.GET.get('matrix_one'))
        m1 = numpy.matrix(m1, numpy.float64)
        m2 = ls.matrix_copy(m1)
        lu = ls.lu(m1)
        context['lu_matrix'] = json.dumps(numpy.array(lu).tolist())
        context['lu_determinant'] = ls.det_of_ludecomposition(lu)
        sol = ls.inverse_subsitution(lu, ls.direct_subsitution(lu, b))
        context['solution'] = json.dumps([sol])
        b.pop(0)
        context['norm'] = format(ls.verify_solution(m2, sol, b), '.10f')
    except Exception as e:
        print(e)
        traceback.print_exc()
        context['error'] = str(e)

    return render_to_response('homework2/problem.html', context)
