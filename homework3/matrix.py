import numpy


def dx1(item):
    if isinstance(item, (numpy.ndarray, numpy.generic)):
        return make_1dx_based_matrix(item)
    if isinstance(item, list):
        return make_1dx_based_list(item)


def length_inc(A):
    n, n = A.shape
    return n


def zlist(n):
    return [0] * n


def dx0(item):
    if isinstance(item, (numpy.ndarray, numpy.generic)):
        return make_0dx_based_matrix(item)
    if isinstance(item, list):
        return make_0dx_based_list(item)


def make_1dx_based_list(lst):
    lst.insert(0, 0)
    return list(lst)


def make_0dx_based_list(lst):
    lst.pop(0)
    return list(lst)


def make_1dx_based_matrix(A):
    """
    Add zero padded column

    Returns:
        new padded matrix
    """
    nr = 1
    n_rows, n_cols = A.shape
    rpad = numpy.zeros((n_cols + nr), numpy.int8)
    arr = A.tolist()

    for l in arr:
        l.insert(0, 0)

    arr.insert(0, list(rpad))
    return numpy.asmatrix(arr)


def make_0dx_based_matrix(A):
    """
    Remove zero padded column

    Returns:
        new padded matrix
    """
    n, n_cols = A.shape
    arr = A.tolist()
    result = []

    nr = 1
    # remove padding
    for i in range(1, n):
        result.append(arr[i][nr:])

    return numpy.asmatrix(result)


def check_divz(nr, eps):
    if not abs(nr) > eps:
        raise Exception("Cannot divide by zero!")


def matrix_copy(m):
    return numpy.asmatrix(numpy.copy(m))


def random_list(high, columns):
    return (numpy.random.randint(high, size=columns)).tolist()


def random_matrix(high, rows, columns):
    a = numpy.asmatrix(numpy.random.randint(high, size=(rows, columns)))
    # convert to float
    return a.astype(numpy.float32, copy=False)


def matrix_vector_mul(A, x):
    n, n = A.shape
    y = [0. for x in range(n)]
    for i in (range(n)):
        for j in (range(n)):
            y[i] += A[i, j] * x[j]
    return y


def get_norm(e):
    n = 0
    for i in e:
        n += i ** 2

    return n


def get_vector_diff(y, b):
    for i in (range(len(y))):
        y[i] -= b[i]

    return y


def solve_sup_triangle_matrix(A, b):
    n, _ = A.shape
    At = A.T
    solutions = []
    for i in range(n):
        s = 0.
        for j in range(i):
            s += At[i, j] * solutions[j]
        xn = (b[i] - s) / At[i, i]
        solutions.append(xn)
    return solutions