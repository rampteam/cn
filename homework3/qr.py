from .matrix import *
import math
import numpy
import timeit
import sys


def ex():
    sys.exit(1)


class QR:
    def __init__(self, eps=None):
        if eps is None:
            eps = 10 ** -4
        self.eps = eps

    def vector_liber(self, A, s):
        A1 = dx1(A)
        s = dx1(s)
        n = length_inc(A1)
        b = zlist(n)
        for i in range(1, n):
            for j in range(1, n):
                b[i] += s[j] * A1[i, j]
        return dx0(b)

    def indentity_matrix(self, n):
        return numpy.matrix(numpy.identity(n), copy=False)

    def qr(self, A0, b):
        A = dx1(A0)
        n = length_inc(A)
        Q = self.indentity_matrix(n)
        b = dx1(b)
        for r in range(1, n - 1):

            u = zlist(n)
            # Pr beta u
            sigma = 0
            for i in range(r, n):
                sigma += A[i, r] ** 2

            if sigma <= self.eps:
                break

            k = math.sqrt(sigma)
            if A[r, r] > 0:
                k = -k

            beta = sigma - k * A[r, r]
            u[r] = A[r, r] - k
            for i in range(r + 1, n):
                u[i] = A[i, r]

            # A = Pr * A
            for j in range(r + 1, n):
                y = 0
                for i in range(r, n):
                    y += u[i] * A[i, j]

                y /= beta

                for i in range(r, n):
                    A[i, j] -= y * u[i]

            # trans col r a matricii A
            A[r, r] = k
            for i in range(r + 1, n):
                A[i, r] = 0

            # b = Pr * b
            y = 0
            for i in range(r, n):
                y += u[i] * b[i]

            y /= beta

            for i in range(r, n):
                b[i] -= y * u[i]

            # Q = Pr * Q
            for j in range(1, n):
                y = 0
                for i in range(r, n):
                    y += u[i] * Q[i, j]

                y /= beta

                for i in range(r, n):
                    Q[i, j] -= y * u[i]

        # print(dx0(A))
        return (dx0(A), dx0(Q).transpose())

    def qr_ls_householder(self, A0, b):
        R, Q = self.qr(A0, b.copy())
        return self.qr_ls(R, Q, b)

    def qr_lib(self, A, s):

        ls = len(s)
        b = numpy.asarray(s).reshape(ls, 1)

        Q, R = numpy.linalg.qr(A)  # qr decomposition of A
        # solve Ax = b using the standard numpy function

        y = numpy.dot(Q.T, b)
        xQR = numpy.linalg.solve(R, y)
        return xQR.T.tolist()[0]

    def qr_ls(self, R, Q, b):
        Qt = Q.transpose()
        n = length_inc(R)
        y = zlist(n)

        for i in (range(n)):
            for j in (range(n)):
                y[i] += Qt[i, j] * b[j]

        return self.inverse_subsitution(R, y)

    def inverse_subsitution(self, A0, b):
        self.validate_substitution(A0)
        A = dx1(A0)
        n = length_inc(A)
        x = zlist(n)
        b = dx1(b)
        for i in reversed(range(1, n)):
            bi = b[i]
            for j in range(i + 1, n):
                bi = bi - A[i, j] * x[j]

            check_divz(A[i, i], self.eps)
            xi = bi / A[i, i]
            x[i] = xi

        return dx0(x)

    def diag_product(self, A):
        # it is the product of the first diagonal
        n, n = A.shape
        d = 1
        for p in range(n):
            d *= A[p, p]
        return d

    def validate_substitution(self, A):
        # matricea trebuie sa fie nesingulara
        if self.diag_product(A) == 0:
            print(self.diag_product(A))
            print(A)
            raise Exception("Matricea trebuie sa fie nesingulara!")

    def compute_inverse(self, A0, b):
        R, Q = self.qr(A0, b)
        n = length_inc(R)
        columns = zlist(n)
        for j in range(n):
            b = Q[j].tolist()[0]
            columns[j] = self.inverse_subsitution(R, b)

        return numpy.asmatrix(columns).transpose()

    def inverse(self, A, b):
        A_inv = self.compute_inverse(A, b)
        A_inv_lib = numpy.linalg.inv(A)
        n = length_inc(A_inv)
        mx = 0
        for i in range(n):
            s = 0
            for j in range(n):
                dff = A_inv[j, j] - A_inv_lib[j, j]
                s += abs(dff)
            mx = max(mx, s)

        return mx

    def get_errors(self, Ainit, binit, s):
        n = length_inc(Ainit)
        # 1
        x_hs = self.qr_ls_householder(Ainit, binit)
        y = matrix_vector_mul(Ainit, x_hs)
        y = get_vector_diff(y, binit)
        e1 = get_norm(y)

        # 2
        x_qr = self.qr_lib(Ainit.tolist(), binit)
        y = matrix_vector_mul(Ainit, x_qr)
        y = get_vector_diff(y, binit)
        e2 = get_norm(y)

        # 3
        y = get_vector_diff(x_hs, s)
        e3 = get_norm(y) / get_norm(s)

        # 4
        y = get_vector_diff(x_qr, s)
        e4 = get_norm(y) / get_norm(s)

        return (e1, e2, e3, e4)
