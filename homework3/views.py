import json
import numpy
import traceback

from django.shortcuts import render_to_response
from .qr import QR
from .matrix import matrix_copy, dx1 as make_1_based
from timeit import timeit


def homework_three(request):
    context = {
        'epsilon': int(request.GET.get('epsilon', 5) if request.GET.get('epsilon', 5) else 0)
    }
    qa = QR(10 ** - context['epsilon'])
    try:
        # ls = LS(10 ** - context['epsilon'])
        s = json.loads(request.GET.get('matrix_two'))
        s = [item for sublist in s for item in sublist]
        matrix_one = json.loads(request.GET.get('matrix_one'))
        m1 = numpy.matrix(matrix_one, numpy.float64)

    except Exception as e:
        print(str(e))
        traceback.print_exc()
        return render_to_response('homework3/problem.html', context)
    try:
        b = qa.vector_liber(matrix_copy(m1), s.copy())
        context['b'] = [b.copy()]

        dx0, dx1 = qa.qr(matrix_copy(m1), b.copy())
        context['r'] = round_matrix(numpy.array(dx0).tolist())
        context['q'] = round_matrix(numpy.array(dx1).tolist())
        context['sol'] = [qa.qr_ls(dx0, dx1, b)]

        # print(qa.qr_ls_householder(matrix_copy(m1), b.copy()))
        # print(qa.qr_lib(matrix_copy(m1), b.copy()))
        
        context['homework_time'] = timeit(lambda: qa.qr_ls_householder(matrix_copy(m1), b.copy()), number=1)
        context['numpy_time'] = timeit(lambda: qa.qr_lib(matrix_copy(m1), b.copy()), number=1)

        errors = qa.get_errors(matrix_copy(m1), b.copy(), s.copy())
        context['errors'] = [format(error, '.40f') for error in errors]

        context['inverse'] = qa.compute_inverse(matrix_copy(m1), b.copy()).tolist()
        context['inverse_lib'] = numpy.linalg.inv(matrix_one).tolist()
        context['inverse_c'] = qa.inverse(matrix_copy(m1), b.copy())

    except Exception as e:
        print(str(e))
        traceback.print_exc()
        context['error'] = str(e)

    return render_to_response('homework3/problem.html', context)


def round_matrix(matrix, decimals=3):
    result = []
    for lin in matrix:
        lin_res = []
        for col in lin:
            lin_res.append(round(col, decimals))
        result.append(lin_res)
    return result
