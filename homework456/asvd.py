from .matrix import *
from .slm import SLM
import sys
import numpy as np
import random


class ASVD:

    def __init__(self, eps=None, kmx=None):
        if eps is None:
            eps = 10 ** -4
        if kmx is None:
            kmx = 10 ** 3
        self.eps = eps
        self.kmx = kmx

    def vector_initial(self, n, low=0, high=100):
        x = []
        for i in range(n):
            val = random.uniform(low, high)
            x.append(val)

        norm_x = get_norm_2(x)
        return mul_vector(x, (1 / norm_x))

    def metoda_puterii(self, A):
        n, m = A.get_shape()
        if not A.is_symmetric():
            raise Exception("Matrix A is not symmetric!")

        vkp1 = self.vector_initial(n)
        w = A.mul_vector(vkp1)
        lkp1 = dot_product(w, vkp1)
        k = 0
        while 1:
            # compute norm here before values are updated
            vkp = mul_vector(vkp1, lkp1)
            cn = get_norm_2(get_vector_diff(w, vkp))

            vkp1 = mul_vector(w, (1 / get_norm_2(w)))
            w = A.mul_vector(vkp1)
            lkp1 = dot_product(w, vkp1)
            k += 1
            if cn <= n * self.eps or k > self.kmx:
                break

        if k > self.kmx:
            # fail
            return None
        else:
            return (lkp1, vkp1)

    def metoda_iteratiei_inverse(self, A, u):
        n, m = A.get_shape()
        if not A.is_symmetric():
            raise Exception("Matrix A is not symmetric!")

        slm = SLM(self.eps, self.kmx)

        vkm1 = self.vector_initial(n)
        k = 0
        while 1:
            k += 1

            # compute A−µIn
            As = A.add_to_diag(-u)
            w = slm.gs_method(As, vkm1)
            vk = mul_vector(w, (1 / get_norm_2(w)))

            z = A.mul_vector(vk)
            lk = dot_product(z, vk)

            vkp = mul_vector(vk, lk)
            cn = get_norm_2(get_vector_diff(z, vkp))

            if cn <= n * self.eps or k > self.kmx:
                break

        if k > self.kmx:
            # fail
            return None
        else:
            return (lk, vk)

    def svd(self, A):

        U, S, V = np.linalg.svd(A, full_matrices=True)

        p, n = S.shape

        svals = []
        mins = sys.float_info.min
        mxs = sys.float_info.max

        for i in range(p):
            if not S[i, i] > 0:
                continue
            v = S[i, i]
            svals.append(v)
            if v > mxs:
                mxs = v

            if v < mins:
                mins = v

        rang = len(svals)
        nr_cond = mxs / mins

        s1 = [[0.0] * p] * n
        S1 = np.matrix(s1, np.float64)
        for i in range(len(svals)):
            S1[i, i] = svals[i]

        x1 = V * S1 * U.T * b

        p = numpy.linalg.pinv(A)

        return svals, rang, nr_cond, None, p, x1
