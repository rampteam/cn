from .sparse_matrix import *
from .slm import SLM
from .asvd import ASVD


def get_file_lines(file):
    with open(file) as f:
        return [line.strip() for line in f.readlines() if line.strip()]


def consume_n(lines):
    return int(lines.pop(0))


def consume_b(lines, n):
    b = []
    for i in range(n):
        b.append(float(lines.pop(0)))
    return b


def get_dok(lines):
    dok = {}
    for line in lines:
        value, i, j = line.split(",")
        value = float(value)
        i = int(i)
        j = int(j)
        dok[(i, j)] = value

    return dok


def get_dok_from_file(file):
    lines = get_file_lines(file)
    n = consume_n(lines)
    b = consume_b(lines, n)
    dok = get_dok(lines)
    sparse = DokSparseMatrix(dok, (n, n))
    return sparse, b


def write_random_lil(fname, n):
    data = random_list_smatrix(n)
    return write_lil_to_file(data, fname)


def write_lil_to_file(data, fname):
    n = len(data)
    with open(fname, 'w') as f:
        f.write(str(n) + "\n\n")
        for i in range(len(data)):
            for j in range(len(data[i])):
                v, col = data[i][j]
                fl = "{:10.10f}, {:5.0f}, {:5.0f}\n".format(v, i, col)
                f.write(fl)

def write_lil_to_file_as_lil(data, fname):
    n = len(data)
    with open(fname, 'w') as f:
        f.write(str(n) + "\n\n")
        for i in range(len(data)):
            f.write('[')
            for j in range(len(data[i])):
                v, col = data[i][j]
                f.write('{:10.10f}, '.format(v))
            f.write(']\n')

# get_csr('in/m_rar_2015_1.txt')
# sparse1, b = get_dok_from_file('in/test.txt')
# sparse2, b = get_dok_from_file('in/test.txt')
# #
# mul = sparse1.mul_matrix(sparse2)
# out(mul)
# mula = sparse1.add_matrix(sparse2)
# mulv = sparse1.mul_vector(b)
# print(mulv)
# out(mula)

# write_random_lil('in/out.txt', 4)



# slm = SLM()
# A, ba = get_dok_from_file('in/m_rar_2015_3.txt')
# n, m = A.shape


# # to ge lil

# lil = dok_to_lil_data(A.add_matrix(A).get_data(), n)
# write_lil_to_file(lil, 'in/add.txt')


# print(A.add_matrix(A))
# print(na + na)
# A, b = get_dok_from_file('in/test_slm.txt')
# x = slm.gs_method(A, b)
# print(x)



# ASVD

# A, ba = get_dok_from_file('in/test_putere.txt')
# svd = ASVD()
# l, v = svd.metoda_iteratiei_inverse(A, -1.5)
# print(l, v)
