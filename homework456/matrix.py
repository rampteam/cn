import numpy
import math


def dx1(item):
    if isinstance(item, (numpy.ndarray, numpy.generic)):
        return make_1dx_based_matrix(item)
    if isinstance(item, list):
        return make_1dx_based_list(item)


def length_inc(A):
    n, n = A.shape
    return n


def zlist(n):
    return [0] * n


def dx0(item):
    if isinstance(item, (numpy.ndarray, numpy.generic)):
        return make_0dx_based_matrix(item)
    if isinstance(item, list):
        return make_0dx_based_list(item)


def make_1dx_based_list(lst):
    lst.insert(0, 0)
    return list(lst)


def make_0dx_based_list(lst):
    lst.pop(0)
    return list(lst)


def make_1dx_based_matrix(A):
    """
    Add zero padded column

    Returns:
        new padded matrix
    """
    nr = 1
    n_rows, n_cols = A.shape
    rpad = numpy.zeros((n_cols + nr), numpy.int8)
    arr = A.tolist()

    for l in arr:
        l.insert(0, 0)

    arr.insert(0, list(rpad))
    return numpy.asmatrix(arr)


def make_0dx_based_matrix(A):
    """
    Remove zero padded column

    Returns:
        new padded matrix
    """
    n, n_cols = A.shape
    arr = A.tolist()
    result = []

    nr = 1
    # remove padding
    for i in range(1, n):
        result.append(arr[i][nr:])

    return numpy.asmatrix(result)


def check_divz(nr, eps):
    if not abs(nr) > eps:
        raise Exception("Cannot divide by zero!")


def matrix_copy(m):
    return numpy.asmatrix(numpy.copy(m))


def random_list(high, columns):
    return (numpy.random.randint(high, size=columns)).tolist()


def random_matrix(high, rows, columns):
    return numpy.asmatrix(numpy.random.randint(high, size=(rows, columns)))


def matrix_vector_mul(A, x):
    n, n = A.shape
    y = [0 for x in range(n)]
    for i in (range(n)):
        for j in (range(n)):
            y[i] += A[i, j] * x[j]

    return y


def get_norm(e):
    n = 0
    for i in e:
        n += i ** 2

    return n


def gauss_using_numpy(A, b, x, n):

    L = np.tril(A)
    U = A - L
    for i in range(n):
        x = np.dot(np.linalg.inv(L), b - np.dot(U, x))
    return x


def get_norm_2(e):
    n = 0
    for i in e:
        n += i ** 2

    return math.sqrt(n)


def get_vector_diff(y, b):
    for i in (range(len(y))):
        y[i] -= b[i]

    return y


def div_vector(v, val):
    for i in range(len(v)):
        v[i] /= val

    return v


def mul_vector(v, val):
    for i in range(len(v)):
        v[i] *= val

    return v


def dot_product(v1, v2):
    n = len(v1)
    if n != len(v2):
        raise Exception("Dot product requires equal length vectors!")
    d = 0.0
    for i in range(n):
        d += v1[i] * v2[i]

    return d
