class SLM:

    def __init__(self, eps=None, kmx=None):
        if eps is None:
            eps = 10 ** -4
        if kmx is None:
            kmx = 10 ** 3
        self.eps = eps
        self.kmx = kmx

    def gs_method(self, A, b):

        diag_check = A.is_nonzero_diag(self.eps)
        if not diag_check:
            raise Exception("Diagonala principala contine elemente zero!")

        n, m = A.get_shape()
        xgs = [0.0] * n

        row_slices = A.get_row_slices()

        k = 0
        while 1:
            norma = 0.0
            for i in range(n):
                i_row = row_slices[i]
                d1 = b[i]
                for j in range(i - 1):
                    d1 -= i_row.get((i, j), 0.0) * xgs[j]

                d2 = 0.0
                for j in range(i + 1, n):
                    d2 += i_row.get((i, j), 0.0) * xgs[j]

                xp = xgs[i]
                self.check_divz(i_row.get((i, i), 0.0))
                xgs[i] = (d1 - d2) / i_row.get((i, i), 0.0)
                dx = xgs[i] - xp
                norma += dx

            k += 1

            # print(k)
            # print(dx)

            if dx < self.eps or k > self.kmx or dx > 10 ** 8:
                break

        if dx < self.eps:
            return xgs
        else:
            raise Exception("Divergenta!")

    def compute_norma(self, A, xgs, b):
        n, m = A.get_shape()
        vmp = A.mul_vector(xgs)
        norma = 0.0
        for i in range(n):
            norma += vmp[i] - b[i]

        return norma

    def check_divz(self, nr):
        if not abs(nr) > self.eps:
            raise Exception("Cannot divide by zero!")
