import copy
import numpy as np
import random


class DokSparseMatrix:
    def __init__(self, arg1=None, shape=None, no_validate=False):
        self.reset()
        self.no_validate = no_validate
        if shape is not None:
            self.shape = shape

        if isinstance(arg1, tuple):  # (M,N)
            M, N = arg1
            self.shape = (M, N)

        if isinstance(arg1, dict):
            self.data = arg1
            self.validate()

        if isinstance(arg1, DokSparseMatrix):
            # its another matrix
            self.data = copy.deepcopy(arg1.get_data())
            self.nnz = arg1.get_nnz()
            self.shape = copy.copy(arg1.get_shape())

    def validate(self):
        if self.no_validate:
            return
        M, N = self.shape
        for i in range(M):
            c = 0
            for j in range(N):
                if self.data.get((i, j), 0) != 0:
                    c += 1
            if c > 10:
                raise Exception("More than 10 non zero elements on a line!")

    def get_nnz(self):
        return self.nnz

    def get_shape(self):
        return self.shape

    def get_data(self):
        return self.data

    def set_data(self, data):
        self.data = data

    def set_shape(self, data):
        self.shape = data

    def __str__(self):
        pps = "A = " + str(self.data)
        pps += "\nNNZ = " + str(self.nnz)
        return pps

    def reset(self):
        self.data = {}
        self.nnz = 0
        self.shape = None

    def update(self, other):
        if isinstance(other, DokSparseMatrix):
            self.data.update(other.get_data())

    def keys(self):
        return self.data.keys()

    def add_matrix(self, other):
        if isinstance(other, DokSparseMatrix):
            if other.shape != self.shape:
                raise ValueError("matrix dimensions are not equal")
            new = DokSparseMatrix(self.shape)
            new.update(self)
            for key in other.keys():
                new.data[key] += other.data[key]
        return new

    def __add__(self, other):
        return self.add_matrix(self)

    def __radd__(self, other):
        return self.__add__(other)

    def __neg__(self):
        new = DokSparseMatrix(self.shape)
        for key in self.keys():
            new.data[key] = -self.data[key]
        return new

    def get_dok_from_column(self, data, i):
        result = {}
        for (key, val) in data.items():
            result[(i, key)] = val

        return result

    def _mul_scalar_with_dok(self, data, other):
        # Multiply this scalar by every element.
        data = copy.deepcopy(data)
        for (key, val) in data.items():
            data[key] = val * other
        return data

    def _add_dok_with_dok(self, d1, d2):
        keys_set = set(list(d1.keys()) + list(d2.keys()))
        data = {}
        for key in keys_set:
            data[key] = d1.get(key, 0) + d2.get(key, 0)

        return data

    def _mul_vector_with_dok(self, data, other):
        # matrix * vector
        result = [0] * self.shape[0]
        for (i, j), v in self.data.items():
            result[i] += v * other[j]
        return result

    def _mul_scalar(self, other):
        # Multiply this scalar by every element.
        new = DokSparseMatrix(self.shape)

        new_dok = self._mul_scalar_with_dok(self.data, other)
        new.set_data(new_dok)
        return new

    def mul_vector(self, other):
        # matrix * vector
        result = [0.0] * self.shape[0]
        for (i, j), v in self.data.items():
            result[i] += v * other[j]
        return result

    def mul_matrix(self, other):
        # matrix * matrix
        rows, cols = self.shape
        result = {}
        for (i, j), v in self.data.items():
            # result[i,:] += v * other[j,:]
            other_dict_j = other.get_dict_slice(j)
            result_i = self.get_dict_slice_from_dok(result, i, cols)
            val_mul = self._mul_scalar_with_dok(other_dict_j, v)
            result_i = self._add_dok_with_dok(result_i, val_mul)
            new_result_i = self.get_dok_from_column(result_i, i)
            result.update(new_result_i)

        return DokSparseMatrix(result, self.shape, True)

    def mul_matrix_lil(self, other):
        dok = self.mul_matrix(other)
        lil = dok_to_lil_data(dok.get_data())
        return lil

    def get_dict_slice_from_dok(self, data, i, max_cols):
        d = {}
        for j in range(max_cols):
            aij = data.get((i, j), 0)
            if aij != 0:
                d[j] = aij

        return d

    def get_row_slice_from_dok(self, data, i, cols):
        d = {}
        for j in range(cols):
            aij = data.get((i, j), 0)
            if aij != 0:
                d[(i, j)] = aij

        return d

    def get_row_slice(self, i):
        rows, cols = self.shape
        return self.get_row_slice_from_dok(self.data, i, cols)

    def get_dict_slice(self, i):
        rows, cols = self.shape
        return self.get_dict_slice_from_dok(self.data, i, cols)

    def is_nonzero_diag(self, eps):
        rows, cols = self.shape
        for i in range(rows):
            if self.data[(i, i)] < eps:
                return False

        return True

    def is_symmetric(self):
        rows, cols = self.shape
        check = False
        for i in range(rows):
            for j in range(i):
                if self.data.get((i, j), 0.0) != self.data.get((j, i), 0.0):
                    check = False
                    return check

        return check

    def add_to_diag(self, u):
        n, m = self.shape
        data = copy.deepcopy(self.data)
        for i in range(n):
            data[(i, i)] = data.get((i, i), 0) + u

        new = DokSparseMatrix(self.shape)
        new.set_data(data)
        return new

    def to_dense_numpy_matrix(self):
        rows, cols = self.shape
        data = [[0.0] * cols] * rows
        for i in range(rows):
            for j in range(cols):
                data[i][j] = self.data.get((i, j), 0)

        return np.matrix(data, np.float64)

    def get_row_slices(self):
        slices = []
        rows, cols = self.shape
        for i in range(rows):
            slices.append(self.get_row_slice(i))

        return slices


def random_dok_smatrix(n, sparse_per=0.2, low=5, high=100):
    nr_zeroes = int(n * sparse_per)
    if nr_zeroes > n:
        raise Exception("Bad sparse percent!")
    data = [0.0] * n
    # overwrite with 1's
    for i in range(n - nr_zeroes):
        val = random.uniform(low, high)
        idx = random.randint(0, n - 1)
        data[idx] = val
    d = {}
    for i in range(n):
        for j in range(i):
            choice = 0.0
            if len(data):
                choice = data.pop(random.randrange(len(data)))
            if choice > 0.0:
                d[(i, j)] = choice
                d[(j, i)] = choice
    # out(DokSparseMatrix(d, (n, n)))
    return d


def random_list_smatrix(n, sparse_per=0.5, low=5, high=100):
    d = random_dok_smatrix(n, sparse_per, low, high)
    return dok_to_lil_data(d, n)


def dok_to_lil_data(d, n):
    l = [[] for x in range(n)]
    for (i, j), v in d.items():
        l[i].append((v, j))
    return l


def out(m):
    M, N = m.get_shape()
    result = []
    line = [0 for j in range(N)]
    for i in range(M):
        result.append(line.copy())
    for i in range(M):
        for j in range(N):
            result[i][j] = m.data.get((i, j), 0)
    return result
