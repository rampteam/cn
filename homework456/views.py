import json
import numpy
import traceback
import os
from django.shortcuts import render_to_response
from .factory import *


def homework_four(request):
    context = {
        'matrix_one': request.GET.get('matrix_one', None),
        'matrix_two': request.GET.get('matrix_two', None),
        'random_size': int(request.GET.get('random_size', 5)),
    }

    matrix_one = context['matrix_one'] if context['matrix_one'] else 'm_rar_2015_3.txt'
    matrix_two = context['matrix_two'] if context['matrix_two'] else 'm_rar_2015_3.txt'
    context['matrix_two'] = matrix_two
    context['matrix_one'] = matrix_one
    try:
        sparse1, b = get_dok_from_file(os.path.dirname(__file__) + '/in/' + matrix_one)
        sparse2, b = get_dok_from_file(os.path.dirname(__file__) + '/in/' + matrix_two)
        mul = sparse1.mul_matrix(sparse2)
        mula = sparse1.add_matrix(sparse2)
        mulv = sparse1.mul_vector(b)
        n, m = mul.shape

        lil = dok_to_lil_data(mul.get_data(), n)
        write_lil_to_file(lil, os.path.dirname(__file__) + '/out/mul.txt')

        lil = dok_to_lil_data(mula.get_data(), n)
        write_lil_to_file(lil, os.path.dirname(__file__) + '/out/addition.txt')

        with open(os.path.dirname(__file__) + '/out/vector.txt', 'w') as f:
            for i in mulv:
                f.write(format(i, '.30f'))
                f.write('\n')

        write_random_lil(os.path.dirname(__file__) + '/out/random.txt', context['random_size'])

    except Exception as e:
        print(str(e))
        traceback.print_exc()
        context['error'] = str(e)
    return render_to_response('homework456/homework4.html', context)


def homework_five(request):
    context = {
        'matrix_one': request.GET.get('matrix_one', None),
        'epsilon': int(request.GET.get('epsilon', 5) if request.GET.get('epsilon', 5) else 0),
        'kmx': int(request.GET.get('kmx', 3) if request.GET.get('kmx', 3) else 0)
    }
    matrix_one = context['matrix_one'] if context['matrix_one'] else 'm_rar_2015_3.txt'
    context['matrix_one'] = matrix_one
    try:
        sparse1, b = get_dok_from_file(os.path.dirname(__file__) + '/in_5/' + matrix_one)
        slm = SLM()
        x = slm.gs_method(sparse1, b)
        context['mul'] = json.dumps([x])
    except Exception as e:
        print(str(e))
        traceback.print_exc()
        context['error'] = str(e)
    return render_to_response('homework456/homework5.html', context)
