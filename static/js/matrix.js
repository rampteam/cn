function Matrix(id, no_edit, no_append, max_columns, update_el) {
    var $el = $('#' + id);
    this.$el = $el;
    var $addColumn = this.$addColumn = $('<button>').attr('id', 'addColumn').text('Adauga rand si coloana').addClass('');
    var $addRow = this.$addRow = $('<button>').attr('id', 'addRow').text('Adauga rand').addClass('expand');
    var $table = $('<table>');
    var $tbody = $('<tbody>');
    var $cell = $('<td>').attr('class', 'matrix-cell');
    $cell.append($('<input>').attr({
        'type': 'text',
        'class': 'matrix-cell-input',
        'value': 0
    }));
    $table.append($tbody);
    //$el.append($addRow);
    $el.append($addColumn);
    if (no_edit !== undefined && no_edit === true) {
        $cell.children().first().prop('disabled', true);
    }

    if (no_append === true) {
        $addRow.css('visibility', 'hidden');
        $addColumn.css('visibility', 'hidden');
    }
    if (no_edit === true) {
        $addRow.css('display', 'none');
        $addColumn.css('display', 'none');

    }
    $el.append($table);
    create(1, 1);

    function create(rows, columns) {
        $tbody.empty();
        $addColumn.unbind('click');
        $addRow.unbind('click');
        for (var i = 0; i < rows; i++) {
            $tbody.append(create_row(columns));
        }

        $addColumn.click(function () {
            if (max_columns !== undefined && $tbody.children().first().children().length >= max_columns) {
                return
            }
            if (update_el !== undefined) {
                update_el.$addColumn.trigger('click');
            }
            add_columns(1);
            $addRow.trigger('click');
        });

        $addRow.click(function () {
            if (update_el !== undefined) {
                update_el.$addRow.trigger('click');
            }
            $tbody.append(create_row($tbody.children().first().children().length));
        });
    }

    function add_columns(number) {
        for (var i = 0; i < number; i++) {
            $tbody.children().each(function (index, el) {
                $(el).append($cell.clone());
            });
        }
    }

    function create_row(columns) {
        var $row = $('<tr>');
        for (var i = 0; i < columns; i++) {
            $row.append($cell.clone());
        }
        return $row;
    }

    this.serialize = function () {
        var content = [];
        $tbody.children().each(function (index, el) {
            var row = [];
            $(el).children().each(function (index, el) {
                var val = parseFloat($(el).find('.matrix-cell-input').first().val(), 10);
                if (val === null) {
                    val = 0;
                }
                row.push(val);
            });
            content.push(row);
        });
        return JSON.stringify(content);
    };

    this.populate = function (data) {
        var rows = data.length;
        var colums = data[0].length;
        create(rows, colums);
        $tbody.children().each(function (index_row, el) {
            $(el).children().each(function (index_col, el) {
                $(el).find('.matrix-cell-input').first().val(data[index_row][index_col]);
            });
        });
    };
    return this;
}